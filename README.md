# Driver API
This project is a microservice for driver crud operations. The application was built using [Clean Architeture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) principles.
It uses the Spring Eureka as service discovery and Zuul as Gateway. 
Get the service discovey [here](https://gitlab.com/rentcar1/servicediscovery).
Get the gataway appplication [here](https://gitlab.com/rentcar1/gateway).


<p style="text-align: center;">
    <img src="arch.png" alt="Architeture" width="300px" heigth="300px">
</p>

## How it works?

* Core = The business logic, Use Cases and models
* Adapter = Integrates core with the application framework
* Framework = Any java web framework, in this case, Spring Boot 2.4.2

## Requirements
* java 11
* Maven
* MySQL Database

## Running the application
1. First create the database. A data.sql with the tables can be found at resources folder.
2. Create the environmental variables:
 *  SERVICE_DISCOVERY_URI - Example: http://localhost:8761/  
 *  MYSQL_HOST - Example: localhost
 *  DB_USERNAME 
 *  DB_PASSWORD
    
2. Start the service discovery application
3. Start the gateway application
3. Run the command:
> mvn spring-boot:run

## Rest API Operations using Proxy

### Get the Bearer Token
> POST localhost:8080/driver-api/login

Request:
```JSON
{
	"username":"test",
	"password":"test"
}
```
200 OK Response:
```JSON
test eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNjExMjc2Mjc1fQ.SIHMUVVBRlvIcQTaFDtwrCidJLlixN03wUVDlklWqVdycufYqaYzg1NSHjPdhB9taS8GHXVAtZuwzBpmTVSpuw
```

### Create a driver
> POST localhost:8080/driver-api/save

Request:
```JSON
 {
 	"cpf":"58403811012"
 }
 ```
200 OK Response:
 ```JSON
 {
   "id": "9694ca34-5e04-42aa-b9cd-029b3c25f9ce",
   "cpf": "58403811012"
 }
 ```

### Update a driver
> PUT localhost:8080/driver-api/update

Request:
 ```JSON
{
	"id":"9694ca34-5e04-42aa-b9cd-029b3c25f9ce",
	"cpf":"98810525051"
}
 ```
200 OK Response:
 ```JSON
 {
   "id": "9694ca34-5e04-42aa-b9cd-029b3c25f9ce",
   "cpf": "98810525051"
 }
 ```

### Get a driver by Id
> GET localhost:8080/driver-api/9694ca34-5e04-42aa-b9cd-029b3c25f9ce

200 OK Response:
 ```JSON
 {
   "id": "9694ca34-5e04-42aa-b9cd-029b3c25f9ce",
   "cpf": "98810525051"
 }
 ```

### Get all drivers
> GET localhost:8080/driver-api/drivers

200 OK Response:
 ```JSON
 [
  {
   "id": "9694ca34-5e04-42aa-b9cd-029b3c25f9ce",
   "cpf": "98810525051"
  }
]
 ```

## Delete a driver by Id
> DELETE localhost:8080/driver-api/9694ca34-5e04-42aa-b9cd-029b3c25f9ce

200 OK Response without body
 