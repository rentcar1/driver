package com.rentcar.driver.core;

import com.rentcar.driver.core.exceptions.DriverNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.UUID;

import static com.rentcar.driver.core.Constants.DRIVER_NOT_FOUND_MSG;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UpdateDriverUseCaseTest {

    @Mock
    private DriverRepository repository;

    @Mock
    private DriverValidator validator;

    private UpdateDriverUseCase updateDriverUseCase;

    @BeforeEach
    public void beforeEach() {
        MockitoAnnotations.openMocks(this);
        updateDriverUseCase = new UpdateDriverUseCase(repository, validator);
    }

    @Test
    @DisplayName("Given existent driver with 11 digits cpf should update the driver")
    public void update_whenValidDCpf_createId() {

        // given
        Driver driver = new Driver(UUID.randomUUID(), "06252652032");

        when(repository.update(driver)).thenReturn(driver);
        when(repository.findById(driver.id)).thenReturn(Optional.of(driver));

        // when
        Driver updatedDriver = updateDriverUseCase.update(driver);

        // then
        assertEquals(driver.id, updatedDriver.id);
        assertEquals(driver.cpf, updatedDriver.cpf);
        verify(repository, times(1)).update(driver);
        verify(repository, times(1)).findById(driver.id);
        verify(validator, times(1)).validate(driver);
    }

    @Test
    @DisplayName("Given non existent driver should throws NotFoundException")
    public void update_whenNonExistent_throwsException() {

        // given
        Driver driver = new Driver(UUID.randomUUID(), "06252652032");

        // when
        when(repository.findById(driver.id)).thenReturn(Optional.empty());
        Exception exception = assertThrows(DriverNotFoundException.class, () -> {
            updateDriverUseCase.update(driver);
        });

        // then
        assertTrue(exception.getMessage().contains(DRIVER_NOT_FOUND_MSG));
        verify(repository, times(0)).update(driver);

    }

}
