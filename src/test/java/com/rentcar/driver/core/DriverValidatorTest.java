package com.rentcar.driver.core;

import com.rentcar.driver.core.exceptions.DriverValidationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static com.rentcar.driver.core.Constants.CPF_INVALID_MSG;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DriverValidatorTest {

    private DriverValidator driverValidator;

    @BeforeEach
    public void beforeEach() {
        driverValidator = new DriverValidator();
    }

    @Test
    @DisplayName("Given a valid cpf with 11 digits should't throws DriverViolationException")
    public void validate_whenValidDriver_createId() {

        // given
        Driver driver = new Driver(UUID.randomUUID(), "06252652032");

        // when
        driverValidator.validate(driver);

        // then no exception is fired.

    }

    @Test
    @DisplayName("Given a invalid cpf with les than 11 digits should throws DriverViolationException")
    public void validate_whenInvalidCpf_createId() {

        // given
        Driver driver = new Driver(UUID.randomUUID(), "6252652032");

        // when
        Exception exception = assertThrows(DriverValidationException.class, () -> {
            driverValidator.validate(driver);
        });

        // then
        assertTrue(exception.getMessage().contains(CPF_INVALID_MSG));
    }

    @Test
    @DisplayName("Given null cpf should throws DriverViolationException")
    public void validate_whenNullCpf_throwsException() {

        // given
        Driver driver = new Driver(null, null);

        // when
        Exception exception = assertThrows(DriverValidationException.class, () -> {
            driverValidator.validate(driver);
        });

        // then
        assertTrue(exception.getMessage().contains(CPF_INVALID_MSG));

    }

    @Test
    @DisplayName("Given an empty cpf should throws DriverViolationException")
    public void validate_whenEmptyCpf_throwsException() {

        // given
        Driver driver = new Driver(null, "");

        // when
        Exception exception = assertThrows(DriverValidationException.class, () -> {
            driverValidator.validate(driver);
        });

        // then
        assertTrue(exception.getMessage().contains(CPF_INVALID_MSG));

    }

    @Test
    @DisplayName("Given a blank cpf should throws DriverViolationException")
    public void validate_whenBlankCpf_throwsException() {

        // given
        Driver driver = new Driver(null, " ");

        // when
        Exception exception = assertThrows(DriverValidationException.class, () -> {
            driverValidator.validate(driver);
        });

        // then
        assertTrue(exception.getMessage().contains(CPF_INVALID_MSG));

    }
}
