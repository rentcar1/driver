package com.rentcar.driver.core;

import com.rentcar.driver.core.exceptions.DriverAlreadyExistsException;
import com.rentcar.driver.core.exceptions.DriverValidationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.UUID;

import static com.rentcar.driver.core.Constants.CPF_ALREADY_EXISTS_MSG;
import static com.rentcar.driver.core.Constants.CPF_INVALID_MSG;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class CreateDriverUseCaseTest {

    @Mock
    private DriverRepository repository;

    @Mock
    private DriverValidator validator;

    private CreateDriverUseCase createDriverUseCase;

    @BeforeEach
    public void beforeEach() {
        MockitoAnnotations.openMocks(this);
        createDriverUseCase = new CreateDriverUseCase(repository, validator);
    }

    @Test
    @DisplayName("Given a valid cpf with 11 digits should create a new driver")
    public void create_whenValidCpf_createId() {

        // given
        Driver driver = new Driver(UUID.randomUUID(), "06252652032");
        Driver createdDriver = new Driver(UUID.randomUUID(), driver.cpf);

        when(repository.create(driver)).thenReturn(createdDriver);
        when(repository.findByCpf(driver.cpf)).thenReturn(Optional.empty());

        // when
        Driver newDriver = createDriverUseCase.create(driver);

        // then
        assertEquals(createdDriver.id, newDriver.id);
        assertEquals(createdDriver.cpf, newDriver.cpf);
        verify(repository, times(1)).create(driver);
        verify(validator, times(1)).validate(driver);
    }

    @Test
    @DisplayName("Given an invalid cpf with les than 11 digits should throw exception and don't create new driver")
    public void create_whenInvalidCpf_createId() {

        // given
        Driver driver = new Driver(UUID.randomUUID(), "6252652032");

        // when
        when(repository.findByCpf(driver.cpf)).thenReturn(Optional.empty());
        doThrow(new DriverValidationException(CPF_INVALID_MSG))
                .when(validator)
                .validate(driver);

        Exception exception = assertThrows(DriverValidationException.class, () -> {
            createDriverUseCase.create(driver);
        });

        // then
        assertTrue(exception.getMessage().contains(CPF_INVALID_MSG));
        verify(repository, times(0)).create(driver);
    }

    @Test
    @DisplayName("Given an existent cpf should throw DriverAlreadyExistsException")
    public void create_whenExistentCpf_shouldThrowException() {

        // given
        Driver driver = new Driver(UUID.randomUUID(), "06252652032");

        // when
        when(repository.findByCpf(driver.cpf)).thenReturn(Optional.of(driver));

        Exception exception = assertThrows(DriverAlreadyExistsException.class, () -> {
            createDriverUseCase.create(driver);
        });

        // then
        assertTrue(exception.getMessage().contains(CPF_ALREADY_EXISTS_MSG));
        verify(repository, times(0)).create(driver);
        verify(repository, times(1)).findByCpf(driver.cpf);
    }

}
