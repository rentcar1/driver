package com.rentcar.driver.core;

import com.rentcar.driver.core.exceptions.DriverNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.UUID;

import static com.rentcar.driver.core.Constants.DRIVER_NOT_FOUND_MSG;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class DeleteDriverUseCaseTest {

    @Mock
    private DriverRepository repository;

    private DeleteDriverUseCase deleteDriverUseCase;

    @BeforeEach
    public void beforeEach() {
        MockitoAnnotations.openMocks(this);
        deleteDriverUseCase = new DeleteDriverUseCase(repository);
    }

    @Test
    @DisplayName("Delete an existent driver")
    public void delete_existentDriver() {

        // given
        Driver driver = new Driver(UUID.randomUUID(), "06252652032");

        // when
        when(repository.findById(driver.id)).thenReturn(Optional.of(driver));
        deleteDriverUseCase.delete(driver.id);

        // then
        verify(repository, times(1)).delete(driver.id);

    }

    @Test
    @DisplayName("Try do delete an non existent driver should throw exceptions")
    public void delete_nonExistentDriver_throwsException() {

        // given
        Driver driver = new Driver(UUID.randomUUID(), "06252652032");

        // when
        when(repository.findById(driver.id)).thenReturn(Optional.empty());
        Exception exception = assertThrows(DriverNotFoundException.class, () -> {
            deleteDriverUseCase.delete(driver.id);
        });

        // then
        assertTrue(exception.getMessage().contains(DRIVER_NOT_FOUND_MSG));
        verify(repository, times(0)).delete(driver.id);
        verify(repository, times(1)).findById(driver.id);

    }

}
