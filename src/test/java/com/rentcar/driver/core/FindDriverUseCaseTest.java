package com.rentcar.driver.core;

import com.rentcar.driver.core.exceptions.DriverNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.rentcar.driver.core.Constants.DRIVER_NOT_FOUND_MSG;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class FindDriverUseCaseTest {

    @Mock
    private DriverRepository repository;

    private FindDriverUseCase findDriverUseCase;

    @BeforeEach
    public void beforeEach() {
        MockitoAnnotations.openMocks(this);
        findDriverUseCase = new FindDriverUseCase(repository);
    }

    @Test
    @DisplayName("Given existent driver ID should retunn a driver")
    public void findById_existentDriver_throwsException() {

        // given
        Driver driver = new Driver(UUID.randomUUID(), "06252652032");

        // when
        when(repository.findById(driver.id)).thenReturn(Optional.of(driver));
        Driver existentDriver = findDriverUseCase.findById(driver.id);

        // then
        assertEquals(driver.id, existentDriver.id);
        assertEquals(driver.cpf, existentDriver.cpf);
        verify(repository, times(1)).findById((driver.id));

    }

    @Test
    @DisplayName("Given non existent driver ID should throws NotFoundException")
    public void findById_nonExistentDriver_throwsException() {

        // given
        Driver driver = new Driver(UUID.randomUUID(), "06252652032");

        // when
        when(repository.findById(driver.id)).thenReturn(Optional.empty());
        Exception exception = assertThrows(DriverNotFoundException.class, () -> {
            findDriverUseCase.findById(driver.id);
        });

        // then
        assertTrue(exception.getMessage().contains(DRIVER_NOT_FOUND_MSG));
        verify(repository, times(1)).findById((driver.id));

    }

    @Test
    @DisplayName("Find all drivers")
    public void findAll_existentDrivers_throwsException() {

        // given
        Driver driver = new Driver(UUID.randomUUID(), "06252652032");
        List<Driver> drivers = new ArrayList<>();
        drivers.add(new Driver(UUID.randomUUID(), "06252652032"));
        drivers.add(new Driver(UUID.randomUUID(), "01393002013"));

        // when
        when(repository.findAllDrivers()).thenReturn(drivers);
        List<Driver> driversList = findDriverUseCase.findAll();

        // then
        assertEquals(2, driversList.size());
        verify(repository, times(1)).findAllDrivers();

    }
}
