drop table if exists driver;

create table driver
(
    driver_id char(36)  primary key,
    cpf varchar(11) not null
);