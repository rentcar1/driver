package com.rentcar.driver.adapter.config;

import com.rentcar.driver.core.*;

/**
 * Specific configuration for Spring Framework.
 */
public class SpringConfig {

    private DriverRepository driverRepository ;

    public SpringConfig(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    public CreateDriverUseCase createDriver(){
        return new CreateDriverUseCase(driverRepository, new DriverValidator());
    }

    public UpdateDriverUseCase updateDriver(){
        return new UpdateDriverUseCase(driverRepository, new DriverValidator());
    }

    public DeleteDriverUseCase deleteDriver(){
        return new DeleteDriverUseCase(driverRepository);
    }

    public FindDriverUseCase findDriver(){
        return new FindDriverUseCase(driverRepository);
    }

}
