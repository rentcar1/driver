package com.rentcar.driver.adapter.controller;

import com.rentcar.driver.core.Driver;

import java.io.Serializable;

public class CreateDriverWeb implements Serializable {

    public final String cpf;

    public CreateDriverWeb() {
        this.cpf = "";
    }

    public CreateDriverWeb(String cpf) {
        this.cpf = cpf;
    }

    public static class Builder {
        private String cpf;

        public CreateDriverWeb.Builder cpf(String cpf) {
            this.cpf = cpf;
            return this;
        }

        public CreateDriverWeb build(){
            return new CreateDriverWeb(cpf);
        }
    }

    public Driver toDriver(CreateDriverWeb driverWeb) {
        return new Driver.Builder()
                .id(null)
                .cpf(cpf)
                .build();
    }

}
