package com.rentcar.driver.adapter.controller;

import com.rentcar.driver.core.CreateDriverUseCase;
import com.rentcar.driver.core.DeleteDriverUseCase;
import com.rentcar.driver.core.FindDriverUseCase;
import com.rentcar.driver.core.UpdateDriverUseCase;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Controller adapter for the driver use cases.
 * It can be coupled to any web framework controller.
 */
public class DriverController {

    private final CreateDriverUseCase createDriver;
    private final FindDriverUseCase findDriver;
    private final UpdateDriverUseCase updateDriver;
    private final DeleteDriverUseCase deleteDriver;

    public DriverController(CreateDriverUseCase createDriver, FindDriverUseCase findDriver, UpdateDriverUseCase updateDriver, DeleteDriverUseCase deleteDriver) {
        this.createDriver = createDriver;
        this.findDriver = findDriver;
        this.updateDriver = updateDriver;
        this.deleteDriver = deleteDriver;
    }

    public DriverWeb createDriver(final CreateDriverWeb createDriverWeb) {
        var driver = createDriverWeb.toDriver(createDriverWeb);
        return DriverWeb.toDriverWeb(this.createDriver.create(driver));
    }

    public DriverWeb updateDriver(final DriverWeb driverWeb) {
        var driver = driverWeb.toDriver(driverWeb);
        return DriverWeb.toDriverWeb(this.updateDriver.update(driver));
    }

    public DriverWeb findById(final UUID id) {
        return DriverWeb.toDriverWeb(this.findDriver.findById(id));
    }

    public void deleteDriver(final UUID id) {
        this.deleteDriver.delete(id);
    }

    public List<DriverWeb> findAll() {
        return findDriver.findAll()
                .stream()
                .map(DriverWeb::toDriverWeb)
                .collect(Collectors.toList());
    }

}
