package com.rentcar.driver.adapter.controller;

import com.rentcar.driver.core.Driver;

import java.io.Serializable;
import java.util.UUID;

/**
 * Web DTO
 */
public class DriverWeb implements Serializable {

    public final UUID id;

    public final String cpf;

    public DriverWeb() {
        this.cpf = "";
        this.id = null;
    }

    public DriverWeb(UUID id, String cpf) {
        this.id = id;
        this.cpf = cpf;
    }

    public static class Builder {
        private UUID id;
        private String cpf;

        public Builder id(UUID id) {
            this.id = id;
            return this;
        }

        public Builder cpf(String cpf) {
            this.cpf = cpf;
            return this;
        }

        public DriverWeb build(){
            return new DriverWeb(id, cpf);
        }
    }

    public Driver toDriver(DriverWeb driverWeb) {
        return new Driver.Builder()
                .id(id)
                .cpf(cpf)
                .build();
    }

    public static DriverWeb toDriverWeb(Driver driver) {
        return new DriverWeb.Builder()
                .id(driver.id)
                .cpf(driver.cpf)
                .build();
    }

}
