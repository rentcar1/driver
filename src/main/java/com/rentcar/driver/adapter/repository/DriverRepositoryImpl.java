package com.rentcar.driver.adapter.repository;

import com.rentcar.driver.framework.spring.repository.DriverEntity;
import com.rentcar.driver.framework.spring.repository.DriverJpaRepository;
import com.rentcar.driver.core.Driver;
import com.rentcar.driver.core.DriverRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class DriverRepositoryImpl implements DriverRepository {

    private DriverJpaRepository jpaRepository;

    public DriverRepositoryImpl(DriverJpaRepository jpaRepository) {
        this.jpaRepository = jpaRepository;
    }

    @Override
    public Driver create(Driver driver) {
        var driverEntity = new DriverEntity(driver.id, driver.cpf);
        driverEntity = jpaRepository.save(driverEntity);
        return new Driver(driverEntity.getId(), driverEntity.getCpf());
    }

    @Override
    public Driver update(Driver driver) {
        var driverEntity = new DriverEntity(driver.id, driver.cpf);
        driverEntity = jpaRepository.save(driverEntity);
        return new Driver(driverEntity.getId(), driverEntity.getCpf());
    }

    @Override
    public Optional<Driver> findById(UUID id) {
        var driverEntity = jpaRepository.findById(id);
        if (driverEntity.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(new Driver(driverEntity.get().getId(), driverEntity.get().getCpf())) ;
    }

    @Override
    public Optional<Driver> findByCpf(String cpf) {
        var driverEntity = jpaRepository.findByCpf(cpf);
        if (driverEntity.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(new Driver(driverEntity.get().getId(), driverEntity.get().getCpf())) ;
    }

    @Override
    public List<Driver> findAllDrivers() {
        return jpaRepository.findAll()
                .stream()
                .map( de -> {
                    var driver = new Driver(de.getId(), de.getCpf());
                    return driver;
                })
                .collect(Collectors.toList());
    }

    @Override
    public void delete(UUID id) {
        jpaRepository.deleteById(id);
    }
}
