package com.rentcar.driver.core;

import com.rentcar.driver.core.exceptions.DriverNotFoundException;

import java.util.List;
import java.util.UUID;

import static com.rentcar.driver.core.Constants.DRIVER_NOT_FOUND_MSG;

public final class FindDriverUseCase {

    private final DriverRepository repository;

    public FindDriverUseCase(DriverRepository repository) {
        this.repository = repository;
    }

    public Driver findById(UUID id) {
        return this.repository.
                findById(id).
                orElseThrow(() -> new DriverNotFoundException((DRIVER_NOT_FOUND_MSG)));
    }

    public List<Driver> findAll() {
        return this.repository.findAllDrivers();
    }
}
