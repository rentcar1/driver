package com.rentcar.driver.core;

public final class Constants {

    private Constants() {}

    public static final String DRIVER_NOT_FOUND_MSG = "Driver not found";

    public static final String CPF_ALREADY_EXISTS_MSG = "Driver cpf already exists";

    public static final String CPF_INVALID_MSG = "Driver cpf is not valid";


}
