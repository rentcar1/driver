package com.rentcar.driver.core;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DriverRepository {

    Driver create(Driver driver);

    Driver update(Driver driver);

    Optional<Driver> findById(UUID id);

    Optional<Driver> findByCpf(String cpf);

    List<Driver> findAllDrivers();

    void delete(UUID id);

}
