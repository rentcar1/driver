package com.rentcar.driver.core;

import com.rentcar.driver.core.exceptions.DriverAlreadyExistsException;
import com.rentcar.driver.core.exceptions.DriverNotFoundException;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.rentcar.driver.core.Constants.CPF_ALREADY_EXISTS_MSG;
import static com.rentcar.driver.core.Constants.DRIVER_NOT_FOUND_MSG;

public final class UpdateDriverUseCase {

    private final DriverRepository repository;

    private final DriverValidator driverValidator;

    private static final Logger LOGGER = Logger.getLogger(UpdateDriverUseCase.class.getName());

    public UpdateDriverUseCase(
            DriverRepository repository,
            DriverValidator validator) {
        this.repository = repository;
        this.driverValidator = validator;
    }

    public Driver update(Driver driver) {

        Optional<Driver> opDriver = repository.findById(driver.id);
        if (opDriver.isEmpty()) {
            LOGGER.log(Level.SEVERE, DRIVER_NOT_FOUND_MSG);
            throw new DriverNotFoundException(DRIVER_NOT_FOUND_MSG);
        }

        driverValidator.validate(driver);

        return this.repository.update(driver);
    }

}
