package com.rentcar.driver.core;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

public class Driver {

    public final UUID id;

    @NotNull(message = "Cpf cannot be null")
    @NotEmpty(message = "Cpf cannot be empty")
    @NotBlank(message = "Cpf cannot be blank")
    @Size(min = 11, max = 11, message = "Invalid size for CPF")
    public final String cpf;

    public Driver(UUID id, String cpf){
        this.id = id;
        this.cpf = cpf;
    }

    public static class Builder {
        private UUID id;
        private String cpf;

        public Driver.Builder id(UUID id) {
            this.id = id;
            return this;
        }

        public Driver.Builder cpf(String cpf) {
            this.cpf = cpf;
            return this;
        }

        public Driver build(){
            return new Driver(id, cpf);
        }
    }

}
