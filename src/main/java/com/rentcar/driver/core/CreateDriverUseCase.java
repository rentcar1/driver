package com.rentcar.driver.core;

import com.rentcar.driver.core.exceptions.DriverAlreadyExistsException;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.rentcar.driver.core.Constants.CPF_ALREADY_EXISTS_MSG;

public final class CreateDriverUseCase {

    private final DriverRepository repository;

    private final DriverValidator driverValidator;

    private static final Logger LOGGER = Logger.getLogger(CreateDriverUseCase.class.getName());

    public CreateDriverUseCase(DriverRepository repository,
                               DriverValidator driverValidator
                               ) {
        this.repository = repository;
        this.driverValidator = driverValidator;
    }

    public Driver create(Driver driver) {

        Optional<Driver> opDriver = repository.findByCpf(driver.cpf);
        if (opDriver.isPresent()) {
            LOGGER.log(Level.SEVERE, CPF_ALREADY_EXISTS_MSG);
            throw new DriverAlreadyExistsException(CPF_ALREADY_EXISTS_MSG);
        }

        driverValidator.validate(driver);
        return this.repository.create(driver);
    }

}
