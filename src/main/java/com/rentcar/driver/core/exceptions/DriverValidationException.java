package com.rentcar.driver.core.exceptions;

public class DriverValidationException extends RuntimeException{
    public DriverValidationException(String message) {
        super(message);
    }
}
