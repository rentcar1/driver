package com.rentcar.driver.core;

import com.rentcar.driver.core.exceptions.DriverNotFoundException;

import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.rentcar.driver.core.Constants.DRIVER_NOT_FOUND_MSG;

public final class DeleteDriverUseCase {

    private final DriverRepository repository;

    public DeleteDriverUseCase(DriverRepository repository) {
        this.repository = repository;
    }

    private static final Logger LOGGER = Logger.getLogger(DeleteDriverUseCase.class.getName());

    public void delete(UUID id) {

        Optional<Driver> opDriver = repository.findById(id);
        if (opDriver.isEmpty()) {
            LOGGER.log(Level.SEVERE, DRIVER_NOT_FOUND_MSG);
            throw new DriverNotFoundException(DRIVER_NOT_FOUND_MSG);
        }

        this.repository.delete(id);
    }
}
