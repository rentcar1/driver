package com.rentcar.driver.framework.spring.controller;

import com.rentcar.driver.adapter.controller.CreateDriverWeb;
import com.rentcar.driver.adapter.controller.DriverController;
import com.rentcar.driver.adapter.controller.DriverWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class SpringDriverController {

    private final DriverController driverController;

    @Autowired
    public SpringDriverController(DriverController driverController) {
        this.driverController = driverController;
    }

    @PostMapping("/save")
    public DriverWeb save(@RequestBody final CreateDriverWeb driverWeb) {

        return driverController.createDriver(driverWeb);
    }

    @PutMapping("/update")
    public DriverWeb update(@RequestBody final DriverWeb driverWeb) {

        return driverController.updateDriver(driverWeb);
    }

    @GetMapping("/drivers")
    public List<DriverWeb> findAll() {

        return driverController.findAll();
    }

    @GetMapping("/{id}")
    public DriverWeb findById(@PathVariable UUID id) {

        return driverController.findById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {
        driverController.deleteDriver(id);
    }

}
