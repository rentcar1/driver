package com.rentcar.driver.framework.spring.repository;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="driver")
public class DriverEntity {

    @Id
    @GeneratedValue(generator = "uuid")
    @Column(name="driver_id", columnDefinition = "char(36)")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    private String cpf;

    public DriverEntity(UUID id, String cpf) {
        this.id = id;
        this.cpf = cpf;
    }

    public DriverEntity() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}
