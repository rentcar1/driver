package com.rentcar.driver.framework.spring.controller;

import com.rentcar.driver.core.exceptions.DriverAlreadyExistsException;
import com.rentcar.driver.core.exceptions.DriverNotFoundException;
import com.rentcar.driver.core.exceptions.DriverValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class SpringControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(
            {
                    DriverValidationException.class,
                    DriverAlreadyExistsException.class,
                    DriverNotFoundException.class
            })
    public ResponseEntity<ErrorResponse> handleError(Exception ex, WebRequest request) {

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setError(ex.getMessage());
        errorResponse.setTimestamp(LocalDateTime.now());
        errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

}
