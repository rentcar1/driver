package com.rentcar.driver.framework.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface DriverJpaRepository extends JpaRepository<DriverEntity, UUID>{

    Optional<DriverEntity> findByCpf(String cpf);
}
