package com.rentcar.driver.framework.spring.config;

import com.rentcar.driver.adapter.config.SpringConfig;
import com.rentcar.driver.adapter.controller.DriverController;
import com.rentcar.driver.adapter.repository.DriverRepositoryImpl;
import com.rentcar.driver.framework.spring.repository.DriverJpaRepository;
import com.rentcar.driver.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Connects the Core to the Spring Application by
 * using adapter layer throw SpringConfig.
 */
@Configuration
public class Config {

    private DriverJpaRepository driverJpaRepository;

    private DriverRepository driverRepository;

    private SpringConfig springConfig;

    @Autowired
    public Config(DriverJpaRepository driverJpaRepository) {
        this.driverJpaRepository = driverJpaRepository;
        this.driverRepository = new DriverRepositoryImpl(this.driverJpaRepository);
        this.springConfig = new SpringConfig(driverRepository);
    }

    @Bean
    public CreateDriverUseCase createDriver() {
        return springConfig.createDriver();
    }

    @Bean
    public FindDriverUseCase findDriver() {
        return springConfig.findDriver();
    }

    @Bean
    public UpdateDriverUseCase updateDriver() {
        return springConfig.updateDriver();
    }

    @Bean
    public DeleteDriverUseCase deleteDriver() {
        return springConfig.deleteDriver();
    }

    @Bean
    public DriverController driverController() {
        return new DriverController(
                createDriver(),
                findDriver(),
                updateDriver(),
                deleteDriver() );
    }

}
